<?php

use Illuminate\Support\Facades\Config;

trait ValidationRules
{

    /**
    *  Returns Validation Rules for Client Create
    *
    * @return array
    */
    public function getCreateClientValidationRules(){
       
        $createValidationRules = [
            'name'=> 'required|max:250',
            'email' => 'required|email|unique:client',
            'phone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'zipcode' => 'required',
            'state_id' => 'required|exists:state,id,status,'.Config::get('constant.status.active'),
            'country_id' => 'required|exists:country,id,status,'. Config::get('constant.status.active'),
            'status' => 'required|in:'.Config::get('constant.status.active').','.Config::get('constant.status.inactive')
        ];

        return $createValidationRules;
    }

    /**
    *  Returns Validation Rules for Client Info Update
    *
    * @return array
    */
    public function getUpdateClientValidationRules($clientId){
        
        $updateValidationRules = [
            'name'=> 'required|max:250',
            'email' => 'required|email|unique:client,email,'.$clientId.',id',
            'phone' => 'required',
            'address' => 'required',
            'city' => 'required',
            'zipcode' => 'required',
            'state_id' => 'required|exists:state,id,status,'.Config::get('constant.status.active'),
            'country_id' => 'required|exists:country,id,status,'. Config::get('constant.status.active'),
            'status' => 'required|in:'.Config::get('constant.status.active').','.Config::get('constant.status.inactive')
        ];

        return $updateValidationRules;
    }


    /**
    *  Returns Validation Rules for Create Site for Customer or Prospect
    *
    * @return array
    */
    public function getCreateClientSiteValidationRules(){

        $createValidationRules = [
            'title'=> 'required|max:250',
            'address' => 'required',
            'city' => 'required',
            'zipcode' => 'required',
            'state_id' => 'required|exists:state,id,status,'.Config::get('constant.status.active'),
            'country_id' => 'required|exists:country,id,status,'. Config::get('constant.status.active'),
            'contactPerson' => 'required|alpha_num_spaces',
            'contactNumber' => 'required',
            'status' => 'required|in:'.Config::get('constant.status.active').','.Config::get('constant.status.inactive')
        ];

        return $createValidationRules;
    }

     /**
    *  Returns Validation Rules for Update Client Site
    *
    * @return array
    */
    public function getUpdateClientSiteValidationRules(){

        $createValidationRules = [
            'title'=> 'required|max:250',
            'address' => 'required',
            'city' => 'required',
            'zipcode' => 'required',
            'state_id' => 'required|exists:state,id,status,'.Config::get('constant.status.active'),
            'country_id' => 'required|exists:country,id,status,'. Config::get('constant.status.active'),
            'contactPerson' => 'required|alpha_num_spaces',
            'contactNumber' => 'required',
            'status' => 'required|in:'.Config::get('constant.status.active').','.Config::get('constant.status.inactive')
        ];

        return $createValidationRules;
    }
    /**
     * Returns Staff validation rules
     * @return array
     */
    public function getStaffValidationUpdateRules($validateSingle=false){
        $bail = ($validateSingle === true)?'bail|':'';
        $validationRules = [
            'firstName'=> sprintf('%srequired|alpha_spaces',$bail),
            'lastName'=> sprintf('%srequired|alpha_spaces',$bail),
            'jobTitle' => sprintf('%srequired|alpha_spaces',$bail),
            'phone' => sprintf('%srequired',$bail),
            'address' => sprintf('%srequired',$bail),
            'city' => sprintf('%srequired',$bail),
            'zipcode' => sprintf('%srequired',$bail),
            'state_id' => sprintf('%srequired|exists:state,id,status,active',$bail),
            'country_id' =>sprintf('%srequired|exists:country,id,status,active',$bail),
            'password' => sprintf('%ssometimes|confirmed|min:6',$bail),
            'profile_image' => sprintf('%ssometimes|image|mimes:jpeg,bmp,png,svg|max:1024',$bail),
        ];
        return $validationRules;
    }

    /**
     * Returns JOB validation rules
     * @return array
     */
    public function getJobValidationRules($validateSingle=false){
        
        $bail = ($validateSingle === true)?'bail|':'';
      
        // TODO:  the after validation rule was not working with combination of current date.
        // TODO: the problem is with the formate that strtotime expect 
        
        $validationRules = [
            'title' => 'required',//|alpha_num_spaces',
            'type' => 'required|in:'. implode(',', Config::get('constant.job.typeRef')),
            'scheduleDate' => 'required|date_format:m/d/Y|after:today',
            'workOrderNo' => 'required|alpha_num',
            'purchaseOrderNo' => 'sometimes',
            'description' => 'required',
            'specialInstruction' => 'sometimes'
        ];
        return $validationRules;
    }

    /**
     * Returns Job update validation rules
     * @return array
     */
    public function getJobUpdateValidationRules($validateSingle=false){
        $bail = ($validateSingle === true)?'bail|':'';
        $validationRules = [
            'title' => 'required',//|alpha_num_spaces',
            'type' => 'required|in:'. implode(',', Config::get('constant.job.typeRef')),
            'scheduleDate' => 'required|date_format:m/d/Y|after:yesterday',
            'description' => 'required',
            'specialInstruction' => 'sometimes'
        ];
        return $validationRules;
    }

    /**
     * Returns Proposal validation rules
     * @return array
     */
    public function getProposalValidationRules($validateSingle = false){
        $bail = ($validateSingle === true)?'bail|':'';
        $validationRules = [
            'title' => $bail.'required|min:5',
            'purchaseOrderNo'=>$bail.'required|max:45',
            'status' => $bail.'required'
        ];
        return $validationRules;
    }
    
    /**
     * Returns Inventory Validation Rules
     * @return array
     */
    public function getInventoryCreateValidationRules($validateSingle=false){
        $bail = ($validateSingle === true)?'bail|':'';
        
        $validationRules = [
            'label'=> $bail.'required|max:250',//alpha_num_space
            'description' => $bail.'required',
            'status' => $bail.'required|in:'.Config::get('constant.status.active').','.Config::get('constant.status.inactive')
        ];
        return $validationRules;
    }
    
    /**
     * Return Report Validation Rules
     * @return array
     */
    
    public function getReportCreateValidationRules($validateSingle=false){
        $bail = ($validateSingle === true)?'bail|':'';
        
        $validationRules = [
            'label'=> $bail.'required|max:250',
            'recommendation' => $bail.'required'
           ];
        return $validationRules;
    }
}
