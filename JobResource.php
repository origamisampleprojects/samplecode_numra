<?php
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;

class JobResource extends Model
{
    use \DataMapper;

    protected $table = "job_resource";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id',
        'staff_id',
        'resource_id',
        'resource_type',
        'quantity'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];

    /**
     * One-to-One Relation with Country
     */
    public function job()
    {
        return $this->belongsTo('App\Http\Models\Job', 'job_id', 'id');
    }

    /**
     * One-to-one Relation with staff
     */
    public function staff()
    {
        return $this->hasOne('App\Http\Models\Staff', 'id', 'staff_id');
    }

    /**
     * Save Job Resource Allocation Data in Job Resource Table
     *
     * @param number $jobId            
     * @param array $jobResourceData            
     * @return
     *
     */
    public static function saveAllocatedResources($jobId, $jobResourceData, $deleteNAddResources = true)
    {
        
        // Delete All resource before adding new
        if ($deleteNAddResources)
            (new JobResource())->deleteByJobID($jobId);
        
        if (array_key_exists('staff', $jobResourceData) && array_get($jobResourceData, 'staff')) {
            
            foreach (array_get($jobResourceData, 'staff') as $staffId) {
                (new JobResource())->fillnSave(self::makeResourceArray($jobId, \Auth::user()->id, $staffId, Config::get('constant.resource.model.staff')));
            }
        }
        
        if (array_key_exists('vehicle', $jobResourceData) && array_get($jobResourceData, 'vehicle')) {
            foreach (array_get($jobResourceData, 'vehicle') as $vehicleId) {
                (new JobResource())->fillnSave(self::makeResourceArray($jobId, \Auth::user()->id, $vehicleId, Config::get('constant.resource.model.vehicle')));
            }
        }
        
        if (array_key_exists('inventory', $jobResourceData) && array_get($jobResourceData, 'inventory')) {
            foreach (array_get($jobResourceData, 'inventory') as $invItemId => $itemQuantity) {
                (new JobResource())->fillnSave(self::makeResourceArray($jobId, \Auth::user()->id, $invItemId, Config::get('constant.resource.model.inventory'), $itemQuantity));
            }
        }
        
        return true;
    }

    /**
     * Assign Resource Data to Job
     *
     * @param number $jobId            
     * @param array $resourceId            
     * @param array $resourceType            
     * @return
     *
     */
    public function assignResource($jobId, $staffId, $resourceId, $resourceType, $createdBy, $quantity = 1)
    {
        $this->fillnSave([
            'job_id' => $jobId,
            'staff_id' => $staffId,
            'resource_id' => $resourceId,
            'resource_type' => $resourceType,
            'quantity' => $quantity
        ]);
        
        return $this;
    }

    /**
     * Get By Resource ID , Type for a Job
     *
     * @param number $jobId            
     * @param array $resourceId            
     * @param array $resourceType            
     * @return
     *
     */
    public function getByJobResourceID($jobId, $resourceId, $resourceType)
    {
        return JobResource::where('job_id', '=', $jobId)->where('resource_id', '=', $resourceId)
            ->where('resource_type', '=', $resourceType)
            ->first();
    }

    public function deleteByJobID($jobId)
    {
        $this->where('job_id', '=', $jobId)->delete();
    }

    public function deleteByTypeNJobID($resourceType, $jobId)
    {
        $this->where('job_id', '=', $jobId)
            ->where('resource_type', '=', $resourceType)
            ->delete();
    }

    /**
     * Make Array of Columns for Job Resource Table
     */
    private static function makeResourceArray($jobId, $staffId, $resourceId, $resourceType, $quantity = 1)
    {
        return [
            'job_id' => $jobId,
            'staff_id' => $staffId,
            'resource_id' => $resourceId,
            'resource_type' => $resourceType,
            'quantity' => $quantity
        ];
    }

    /**
     * Get Jobs for given Staff Member
     * This method is part of mobile api, which returns paginated listing of
     * jobs which are assigned to a staff.
     *
     * @param Staff $staffResource            
     * @param string $status            
     * @param string $search            
     * @param number $currentPage            
     * @param number $perPage            
     * @param bool $assigned            
     *
     * @return Job Collection | NULL App/Http/Models/Job
     */
    public function getJobs(Staff $staffResource, $status, $search, $currentPage, $perPage, $assigned = true)
    {
        $jobIds = $this->where('job_resource.resource_type', '=', get_class($staffResource))
            ->where('job_resource.resource_id', '=', $staffResource->id)
            ->get()
            ->lists('job_id');
        $query = Job::where(function ($query) use($assigned, $jobIds) {
            if ($assigned) {
                $query->whereIn('job.id', $jobIds);
            } else {
                $query->whereNotIn('job.id', $jobIds);
            }
        })->whereIn('job.status', $status)
            ->where(function ($query) use($search) {
            if ($search)
                $query->where('job.title', 'like', "%$search%");
        })
            ->join('client_site', function ($join) {
            $join->on('client_site.id', '=', 'job.client_site_id');
        });
        if (in_array(\Config::get('constant.status.inprogress'), $status)) {
            $query->orderByRaw(\DB::raw("FIELD(job.status,'" . implode("','", $status) . "') ASC"));
        }
        return $query->groupBy('job.id')->paginate($perPage, [
            'job.id',
            'job.title',
            'job.scheduled_at',
            'job.type',
            'job.status',
            'client_site.contactPerson',
            'client_site.contactNumber'
        ], 'page', $currentPage);
        
    }

    /**
     * Get a resource for a job with specified resource type
     *
     * @param int $job_id            
     * @param object $resource            
     * @return Eloquent Object
     */
    public function getResourceForJob($job_id, $resource)
    {
        return $this->where('job_id', '=', $job_id)
            ->where('resource_id', '=', $resource->id)
            ->where('resource_type', '=', get_class($resource))
            ->first();
    }
}
