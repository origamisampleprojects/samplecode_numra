<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class JobNote extends Model
{
    use \DataMapper;

    protected $table = "job_note";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'job_id',
        'staff_id',
        'action',
        'note'
    ];

    /**
     * The attributes that are hidden.
     *
     * @var array
     */
    protected $hidden = [
        'updated_at',
        'deleted_at'
    ];

    public function staff(){
    	return $this->belongsTo('App\Http\Models\Staff')->addSelect(['id','firstName', 'lastName', 'profileImage', 'jobTitle']);
    }

    public function saveNote($jobId, $staffId, $action, $noteText){
    	$jobNoteArr = [
    		'job_id' => $jobId,
	        'staff_id' => $staffId,
	        'action' => $action,
	        'note' => $noteText
    	];
    	return $this->fillnSave($jobNoteArr);
    }

    public static function isJobCompletionNoteAdded($jobId, $staffId){
        $staffJobNoteObj = self::where('job_id', '=', $jobId)
            ->where('staff_id', '=', $staffId)
            ->where('action', '=', config('constant.status.completed'))
            ->first();
        return ($staffJobNoteObj instanceOf JobNote)? true : false;
    }
    
    
}
