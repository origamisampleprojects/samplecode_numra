<?php
namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Models\Job;
use App\Http\Models\Staff;
use App\Http\Models\State;
use App\Http\Models\Client;
use App\Http\Models\Country;
use App\Http\Models\Vehicle;
use Illuminate\Http\Request;
use App\Http\Models\Document;
use App\Http\Models\Proposal;
use App\Http\Libraries\Common;
use App\Http\Models\ClientSite;
use App\Http\Models\JobTimesheet;
use App\Http\Models\InventoryItem;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;

class JobController extends Controller
{
    use \ValidationRules;

    public function index()
    {
        try {
            $search = Input::has('search') ? Input::get('search') : '';
            $filter = (Input::has('filter') && Input::get('filter') != 'all') ? Input::get('filter') : '';
            $currentPage = Input::has('page') ? Input::get('page') : 0;
            $perPage = Config::get('app.portal_items_per_page');
            
            $appendArr = array();
            if ($search) {
                $appendArr['search'] = $search;
            }
            
            if ($filter) {
                $appendArr['filter'] = $filter;
            }
            
            $jobListingObj = new Job();
            $jobListingObj = $jobListingObj->getPaginatedList($search, $filter, $currentPage, $perPage);
            
            // dd($jobListingObj->toArray());
            
            if ($jobListingObj->count() == 0 && $jobListingObj->total() > 0) {
                return \Redirect::to($jobListingObj->appends($appendArr)->url($jobListingObj->lastPage()));
            }
            
            return \View::make('job.listing', compact('jobListingObj', 'appendArr', 'search', 'filter', 'currentPage'));
        } catch (Exception $exp) {
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput()->with([
                'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

    /**
     * Create a view for creating a new job
     */
    public function selectJobFor()
    {
        // List of proposal
        // List of Client
        // List of Client Site
        $clientListObj = Client::listAll();
        
        $clientSiteListObj = NULL;
        $clientId = NULL;
        
        if ($clientListObj && $clientListObj->count()) {
            $clientId = $clientListObj->first()->id;
            $clientType = $clientListObj->first()->isOfType();
            $clientSiteListObj = ClientSite::listByClientID($clientListObj->first()->id);
        }
        
        $clientListObj = array(
            Config::get('constant.client.type.customer') => Client::listAll(Config::get('constant.client.type.customer')),
            Config::get('constant.client.type.prospect') => Client::listAll(Config::get('constant.client.type.prospect'))
        );
        
        $proposalListObj = Proposal::listAll();
        
        return \View::make('job.select', compact('clientId', 'clientType', 'clientListObj', 'clientSiteListObj', 'proposalListObj'));
    }

    /**
     * Store source for creating a new job
     *
     * @param Request $request            
     * @return Redirect
     */
    public function storeJobCreationSource(Request $request)
    {
        try {
            
            if (! $request->has('source')) {
                return \Redirect::back()->with([
                    'serverError' => trans('messages.portal.error.job_create_session_error')
                ]);
            }
            
            $jobSourceArr = [];
            
            switch ($request->get('source')) {
                case 'client':
                    $jobSourceArr = [
                        'source' => $request->get('source'),
                        'client_id' => $request->get('client_id'),
                        'client_site_id' => $request->get('client_site_id')
                    ];
                    break;
                
                case 'proposal':
                    $jobSourceArr = [
                        'source' => $request->get('source'),
                        'proposal_id' => $request->get('proposal_id')
                    ];
                    
                    break;
            }
            
            \Session::put('jobSource', $jobSourceArr);
            
            return \Redirect::route('portal.job.create');
        } catch (Exception $exp) {
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput($request->all())
                ->with([
                'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

    /**
     * Create a view for creating new job
     */
    public function create(Request $request)
    {
        // Client Site Info
        // Client Info
        // List of vehicle
        // List of Technician
        // List of Inventory item
        $staffIDList = Staff::getStaffIDList();
        
        if (! \Session::has('jobSource.source')) {
            return \Redirect::route('portal.job.select.source')->with([
                'serverError' => trans('messages.portal.error.job_create_session_error')
            ]);
        }
        
        $jobSource = \Session::get('jobSource.source');
        
        $proposalId = '';
        $proposalObj = NULL;
        $purchaseOrderNo = '';

        switch ($jobSource) {
            
            case 'client':
                $siteId = \Session::get('jobSource.client_site_id');
                break;
            
            case 'proposal':
                $proposalId = \Session::get('jobSource.proposal_id');
                $proposalObj = (new Proposal())->getByID($proposalId);
                
                if (! $proposalObj instanceof Proposal) {
                    return \Redirect::route('portal.job.select.source')->with([
                        'serverError' => trans('messages.portal.error.job_create_only_for_valid_proposal')
                    ]);
                }
                $proposalObj->load('client');
                $proposalId = $proposalObj->id;
                $siteId = $proposalObj->client_site_id;
                break;
        }
        
        try {
            $clientSiteObj = new ClientSite();
            $clientSiteObj = $clientSiteObj->getByID($siteId);
            
            if (! $clientSiteObj instanceof ClientSite) {
                return \Redirect::route('portal.job.select.source')->with([
                    'serverError' => trans('messages.portal.error.job_create_only_for_valid_client_site')
                ]);
            }
            
            $clientSiteObj->load('client', 'state', 'country');
            
            if (! $clientSiteObj->client instanceof Client) {
                return \Redirect::route('portal.job.select.source')->with([
                    'serverError' => trans('messages.portal.error.job_create_only_for_valid_client')
                ]);
            }
            
            $jobCount = Job::getJobCountByClientId($clientSiteObj->client->id);
            
            $staffListObj = Staff::listAllByType(Config::get('constant.staff.type.technician'));
            $vehicleListObj = Vehicle::listAll();
            $inventoryItemListObj = InventoryItem::listAll();
            
            $workOrderNo = sprintf('%s%s%s', Common::firstLetters($clientSiteObj->client->name), Carbon::now()->format('Ymd'), sprintf('%02s', ($jobCount + 1)));
            
            


            // Creating Array of Selected inventory Items
            $selectedItemsArr = [];
            if ($request->old('inventory') && is_array($request->old('inventory'))) {
                
                $selectedKeys = array_keys($request->old('inventory'));
                
                foreach ($inventoryItemListObj->toArray() as $invItem) {
                    if (in_array(array_get($invItem, 'id'), $selectedKeys)) {
                        $invItem['quantity'] = array_get($request->old('inventory'), array_get($invItem, 'id'));
                        $selectedItemsArr[] = $invItem;
                    }
                }
            }
            
            return \View::make('job.create', compact('clientSiteObj', 'staffListObj', 'vehicleListObj', 'inventoryItemListObj', 'proposalObj', 'selectedItemsArr', 'proposalId', 'workOrderNo', 'purchaseOrderNo'));
        } catch (Exception $exp) {
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput($request->all())
                ->with([
                'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

    /**
     * Store new job
     */
    public function store(Request $request)
    {

        if (! \Session::has('jobSource.source')) {
            return \Redirect::route('portal.job.select.source')->with([
                'serverError' => trans('messages.portal.error.job_create_session_error')
            ]);
        }
        
        $validationRules = self::getJobValidationRules();
        
        $validator = \Validator::make($request->all(), $validationRules);
        
        if ($validator->fails()) {
            return \Redirect::back()->withInput($request->all())
                ->withErrors($validator);
        }
        
        try {
            $scheduleDate = Carbon::createFromFormat('m/d/Y', array_get($request->all(), 'scheduleDate'));
            $scheduleDate = $scheduleDate->startOfDay();
            if ($scheduleDate->lt(Carbon::yesterday())) {
                $validator->errors()->add('scheduleDate', trans('messages.portal.error.job_schedule_date_older_than_today'));
                return \Redirect::back()->withInput($request->all())
                    ->withErrors($validator);
            }
        } catch (Exception $exp) {
            $validator->errors()->add('scheduleDate', trans('messages.portal.error.job_schedule_date_invalid_format'));
            return \Redirect::back()->withInput($request->all())
                ->withErrors($validator);
        }
        
        $jobSource = \Session::get('jobSource.source');
        
        $proposalId = '';
        $proposalObj = NULL;
        
        switch ($jobSource) {
            
            case 'client':
                $siteId = \Session::get('jobSource.client_site_id');
                break;
            
            case 'proposal':
                $proposalId = \Session::get('jobSource.proposal_id');
                $proposalObj = (new Proposal())->getByID($proposalId);
                
                if (! $proposalObj instanceof Proposal) {
                    return \Redirect::route('portal.job.select.source')->with([
                        'serverError' => trans('messages.portal.error.job_create_only_for_valid_proposal')
                    ]);
                }
                
                $proposalId = $proposalObj->id;
                $siteId = $proposalObj->client_site_id;
                break;
        }
        
        try {
            $clientSiteObj = new ClientSite();
            $clientSiteObj = $clientSiteObj->getByID($siteId);
            
            if (! $clientSiteObj instanceof ClientSite) {
                return \Redirect::route('portal.job.select.source')->with([
                    'serverError' => trans('messages.portal.error.job_create_only_for_valid_client_site')
                ]);
            }
            
            $clientSiteObj->load('client');
            
            if (! $clientSiteObj->client instanceof Client) {
                return \Redirect::route('portal.job.select.source')->with([
                    'serverError' => trans('messages.portal.error.job_create_only_for_valid_client')
                ]);
            }
            
            \DB::beginTransaction();
            
            $jobFilteredData = $request->only('title', 'type', 'description', 'workOrderNo', 'purchaseOrderNo','scheduleDate', 'description', 'specialInstruction', 'productType', 'tankSize', 'hoseLength', 'openingSize', 'drum', 'shaftDepth','personal', 'personalEquip','material', 'equipment', 'utilityCheck', 'utilityNo', 'permit', 'notification', 'inspection', 'inspectionOn', 'subcontractor', 'jobMaterial');
            
            $jobFilteredData['utilityCheck'] = (array_get( $jobFilteredData, 'utilityCheck'))? config::get('constant.option.true') : Config::get('constant.option.false');
            $jobFilteredData['permit'] = (array_get( $jobFilteredData, 'permit'))? config::get('constant.option.true') : Config::get('constant.option.false');
            $jobFilteredData['notification'] = (array_get( $jobFilteredData, 'notification'))? config::get('constant.option.true') : Config::get('constant.option.false');
            $jobFilteredData['inspection'] = (array_get( $jobFilteredData, 'inspection'))? config::get('constant.option.true') : Config::get('constant.option.false');
            $jobFilteredData['subcontractor'] = (array_get( $jobFilteredData, 'subcontractor'))? config::get('constant.option.true') : Config::get('constant.option.false');
            $jobFilteredData['jobMaterial'] = (array_get( $jobFilteredData, 'jobMaterial'))? config::get('constant.option.true') : Config::get('constant.option.false');

            $jobResourceFilteredData = $request->only('staff', 'vehicle', 'inventory');
            
            $jobObj = new Job();
            
            $jobObj->setAssociationData($clientSiteObj->client->id, $clientSiteObj->id, $proposalId, $scheduleDate)->fillnSave($jobFilteredData);
            
            $jobObj->allocatedResources($jobResourceFilteredData);
            
            // Converting Prospect to Customer
            if (! $clientSiteObj->client->isCustomer()) {
                $clientSiteObj->client->convertProspectToCustomer();
            }
            
            if ($proposalObj && ! $proposalObj->isAccepted()) {
                $proposalObj->updateStatus(Config::get('constant.status.accepted'));
            }
            
            \DB::commit();
            
            return \Redirect::route('portal.job.listing')->with([
                'success' => trans('messages.portal.success.job_created_successfully')
            ]);
        } catch (Exception $exp) {
            \DB::rollback();
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput($request->all())
                ->with([
                'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

    /**
     * View for Editing a Job
     */
    public function edit(Request $request, $jobId)
    {
        // Client Site Info
        // Client Info
        // List of vehicle
        // List of Technician
        // List of Inventory item
        $jobObj = new Job();
        $jobObj = $jobObj->getByID($jobId);
        
        if (! $jobObj instanceof Job) {
            return \Redirect::route('portal.job.listing')->with([
                'serverError' => trans('messages.portal.error.job_not_found')
            ]);
        }
        
        $jobObj->load('client', 'clientSite', 'proposal', 'document', 'note', 'note.staff');
        
        if (! $jobObj->client instanceof Client) {
            return \Redirect::route('portal.job.listing')->with([
                'serverError' => trans('messages.portal.error.job_belongs_to_invalid_client')
            ]);
        }
        
        if (! $jobObj->clientSite instanceof ClientSite) {
            return \Redirect::route('portal.job.listing')->with([
                'serverError' => trans('messages.portal.error.job_belongs_to_invalid_client_site')
            ]);
        }
        
        if ($jobObj->proposal_id && ! $jobObj->proposal instanceof Proposal) {
            return \Redirect::route('portal.job.listing')->with([
                'serverError' => trans('messages.portal.error.job_belongs_to_invalid_proposal')
            ]);
        }
        
        if ($jobObj->isCancelled() || $jobObj->isCompleted())
            return \Redirect::route('portal.job.view', $jobObj->id)->with([
                'serverError' => trans('messages.portal.error.job_edit_not_allowed')
            ]);
        
        try {
            
            $jobResources = $jobObj->getResources();
            
            if ($request->old('staff')) {
                $jobStaffResources = old('staff');
            } else {
                $jobStaffResources = Common::mapArrayColumn(array_get($jobResources, 'staff'));
            }
            
            if ($request->old('vehicle')) {
                $jobVehicleResources = old('vehicle');
            } else {
                $jobVehicleResources = Common::mapArrayColumn(array_get($jobResources, 'vehicle'));
            }
            
            $staffListObj = Staff::listAllByType(Config::get('constant.staff.type.technician'));
            $vehicleListObj = Vehicle::listAll();
            $inventoryItemListObj = InventoryItem::listAll();
            
            // Creating Array of Selected inventory Items
            $selectedItemsArr = [];
            
            if ($request->old('inventory') && is_array($request->old('inventory'))) {
                $selectedKeys = array_keys($request->old('inventory'));
                foreach ($inventoryItemListObj->toArray() as $invItem) {
                    if (in_array(array_get($invItem, 'id'), $selectedKeys)) {
                        $invItem['quantity'] = array_get($request->old('inventory'), array_get($invItem, 'id'));
                        $selectedItemsArr[] = $invItem;
                    }
                }
            } else {
                $selectedItemsArr = Common::mapInventoryItemColumn(array_get($jobResources, 'inventory'));
            }
            
            return \View::make('job.edit', compact('staffListObj', 'jobStaffResources', 'vehicleListObj', 'jobVehicleResources', 'inventoryItemListObj', 'jobObj', 'selectedItemsArr'));
        } catch (\Exception $exp) {
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput($request->all())
                ->with([
                'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

    /**
     * Update Job Resource
     */
    public function update(Request $request, $jobId)
    {
        $validationRules = self::getJobUpdateValidationRules();

        try {
            $jobObj = new Job();
            $jobObj = $jobObj->getByID($jobId);
            
            if (! $jobObj instanceof Job) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_not_found')
                ]);
            }
        }
        catch(Exception $exp){
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput($request->all())
                ->with([
                'serverError' => trans('messages.portal.error.server_error')
            ]);
        }

        if( $jobObj->isInprogress() ) {
            $validationRules = array_except($validationRules,['title', 'type', 'scheduleDate']);
        }

        $validator = \Validator::make($request->all(), $validationRules);
        
        if ($validator->fails()) {
            return \Redirect::back()->withInput($request->all())
                ->withErrors($validator);
        }
    

        //dd($request->all());

        try {
            if(array_get($request->all(), 'scheduleDate') && $jobObj->isPending()){
                $scheduleDate = Carbon::createFromFormat('m/d/Y', array_get($request->all(), 'scheduleDate'));
                $scheduleDate = $scheduleDate->startOfDay();
                if ($scheduleDate->lt(Carbon::yesterday())) {
                    $validator->errors()->add('scheduleDate', trans('messages.portal.error.job_schedule_date_older_than_today'));
                    return \Redirect::back()->withInput($request->all())
                        ->withErrors($validator);
                }
            }
        } catch (Exception $exp) {
            $validator->errors()->add('scheduleDate', trans('messages.portal.error.job_schedule_date_invalid_format'));
            return \Redirect::back()->withInput($request->all())
                ->withErrors($validator);   
        }
        
        //dd($scheduleDate);

        try {
            $jobObj = new Job();
            $jobObj = $jobObj->getByID($jobId);
            
            if (! $jobObj instanceof Job) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_not_found')
                ]);
            }
            
            $jobObj->load('client', 'clientSite', 'proposal');
            
            if (! $jobObj->client instanceof Client) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_client')
                ]);
            }
            
            if (! $jobObj->clientSite instanceof ClientSite) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_client_site')
                ]);
            }
            
            if ($jobObj->proposal_id && ! $jobObj->proposal instanceof Proposal) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_proposal')
                ]);
            }
            
            if ($jobObj->isCancelled() || $jobObj->isCompleted())
                return \Redirect::route('portal.job.view', $jobObj->id)->with([
                    'serverError' => trans('messages.portal.error.job_edit_not_allowed')
                ]);
            
            \DB::beginTransaction();
            
            if ($jobObj->isPending()) {
                
                $jobFilteredData = $request->only('title', 'type', 'description', 'purchaseOrderNo', 'description', 'specialInstruction', 'productType', 'tankSize', 'hoseLength', 'openingSize', 'drum', 'shaftDepth','personal', 'personalEquip','material', 'equipment', 'utilityCheck', 'utilityNo', 'permit', 'notification', 'inspection', 'inspectionOn', 'subcontractor', 'jobMaterial');

                $jobFilteredData['scheduled_at'] = $scheduleDate;
                $jobFilteredData['utilityCheck'] = (array_get( $jobFilteredData, 'utilityCheck'))? config::get('constant.option.true') : Config::get('constant.option.false');
                $jobFilteredData['permit'] = (array_get( $jobFilteredData, 'permit'))? config::get('constant.option.true') : Config::get('constant.option.false');
                $jobFilteredData['notification'] = (array_get( $jobFilteredData, 'notification'))? config::get('constant.option.true') : Config::get('constant.option.false');
                $jobFilteredData['inspection'] = (array_get( $jobFilteredData, 'inspection'))? config::get('constant.option.true') : Config::get('constant.option.false');
                $jobFilteredData['subcontractor'] = (array_get( $jobFilteredData, 'subcontractor'))? config::get('constant.option.true') : Config::get('constant.option.false');
                $jobFilteredData['jobMaterial'] = (array_get( $jobFilteredData, 'jobMaterial'))? config::get('constant.option.true') : Config::get('constant.option.false');
                

                $jobResourceFilteredData = $request->only('staff', 'vehicle', 'inventory');

            } else 
                if ($jobObj->isInprogress()) {
                    $jobFilteredData = $request->only('purchaseOrderNo', 'description', 'specialInstruction', 'productType', 'tankSize', 'hoseLength', 'openingSize', 'drum', 'shaftDepth','personal', 'personalEquip','material', 'equipment', 'utilityCheck', 'utilityNo', 'permit', 'notification', 'inspection', 'inspectionOn', 'subcontractor', 'jobMaterial');

                    $jobFilteredData['utilityCheck'] = (array_get( $jobFilteredData, 'utilityCheck'))? config::get('constant.option.true') : Config::get('constant.option.false');
                    $jobFilteredData['permit'] = (array_get( $jobFilteredData, 'permit'))? config::get('constant.option.true') : Config::get('constant.option.false');
                    $jobFilteredData['notification'] = (array_get( $jobFilteredData, 'notification'))? config::get('constant.option.true') : Config::get('constant.option.false');
                    $jobFilteredData['inspection'] = (array_get( $jobFilteredData, 'inspection'))? config::get('constant.option.true') : Config::get('constant.option.false');
                    $jobFilteredData['subcontractor'] = (array_get( $jobFilteredData, 'subcontractor'))? config::get('constant.option.true') : Config::get('constant.option.false');
                    $jobFilteredData['jobMaterial'] = (array_get( $jobFilteredData, 'jobMaterial'))? config::get('constant.option.true') : Config::get('constant.option.false');

                    $jobResourceFilteredData = NULL;
                }
                
            //dd($jobFilteredData);

            $jobObj->fillnSave($jobFilteredData);
            
            if ($jobResourceFilteredData && $jobObj->isPending()) {
                $jobObj->allocatedResources($jobResourceFilteredData);
            }
            
            \DB::commit();
            
            return \Redirect::route('portal.job.listing')->with([
                'success' => trans('messages.portal.success.job_updated_successfully')
            ]);
        } catch (Exception $exp) {
            \DB::rollback();
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput($request->all())
                ->with([
                'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

    /**
     * Create a view for creating new job
     */
    public function preview(Request $request, $jobId)
    {
        // Client Site Info
        // Client Info
        // List of vehicle
        // List of Technician
        // List of Inventory item
        try {
            $jobObj = new Job();
            $jobObj = $jobObj->getByID($jobId);
            
            if (! $jobObj instanceof Job) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_not_found')
                ]);
            }
            
            $jobObj->load('client', 'clientSite', 'proposal', 'document', 'note', 'note.staff');
            
            if (! $jobObj->client instanceof Client) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_client')
                ]);
            }
            
            if (! $jobObj->clientSite instanceof ClientSite) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_client_site')
                ]);
            }
            
            if ($jobObj->proposal_id && ! $jobObj->proposal instanceof Proposal) {
                return \Redirect::route('portal.job.listing')->with([
                    'serverError' => trans('messages.portal.error.job_belongs_to_invalid_proposal')
                ]);
            }
            
            $jobResources = $jobObj->getResources();
            $jobStaffResource = NULL;
            
            if (array_get($jobResources, 'staff')) {
                
                $jobStaffResource = JobTimesheet::getJobRelatedTimesheets($jobId, array_get($jobResources, 'staff'));
                $jobResources['staff'] = $jobStaffResource;
            }
            
            $this->calculateJobTime($jobResources);
            
            // $jobNotes = NULL;
            
            return \View::make('job.preview', compact('jobObj', 'jobResources', 'jobNotes'));
        } catch (Exception $exp) {
            \DB::rollback();
            self::logError($exp, __CLASS__, __METHOD__);
            return \Redirect::back()->withInput($request->all())
                ->with([
                'serverError' => trans('messages.portal.error.server_error')
            ]);
        }
    }

    /**
     * Cancel job
     */
    public function cancel(Request $request, $jobId)
    {
        try {
            $jobObj = new Job();
            $jobObj = $jobObj->getByID($jobId);
            
            if (! $request->has('note')) {
                return \Response::json(array(
                    'code' => trans('messages.portal.error.ajax.job_cancellation_note_not_present.code'),
                    'body' => trans('messages.portal.error.ajax.job_cancellation_note_not_present.message')
                ), 412);
            }
            
            if (! $jobObj instanceof Job) {
                return \Response::json(array(
                    'code' => trans('messages.portal.error.ajax.job_not_found.code'),
                    'body' => trans('messages.portal.error.ajax.job_not_found.message')
                ), 404);
            }
            
            if ($jobObj->status != Config::get('constant.status.pending')) {
                
                return \Response::json(array(
                    'code' => trans('messages.portal.error.ajax.job_cancelled_only_for_pending_job.code'),
                    'body' => trans('messages.portal.error.ajax.job_cancelled_only_for_pending_job.message')
                ), 404);
            }
            
            \DB::beginTransaction();
            
            $jobObj = $jobObj->updateStatus(Config::get('constant.status.cancelled'));
            $jobObj->addNote(Auth::user()->id, config('constant.status.cancelled'), strip_tags($request->get('note')));
            \DB::commit();
            return \Response::json(array(
                'code' => trans('messages.portal.success.ajax.job_cancelled_successfully.code'),
                'body' => trans('messages.portal.success.ajax.job_cancelled_successfully.message')
            ), 200);
        } catch (Exception $exp) {
            \DB::rollback();
            self::logError($exp, __CLASS__, __METHOD__);
            return \Response::json(array(
                'code' => 200,
                'body' => trans('messages.portal.error.server_error')
            ), 500);
        }
    }

    /**
     * Calculate Job Time using Staff Timesheets
     * This function calculate and set job time i.e.
     * total, billable , nonbillable .
     * It also add totalTime taken by each staff using the timesheet
     *
     * @param
     *            array &$jobResourceArr
     */
    private function calculateJobTime(&$jobResourceArr)
    {
        $jobResourceArr['time'] = $jobTime = array(
            'total' => 0,
            'billable' => 0,
            'nonbillable' => 0
        );
        
        if (array_get($jobResourceArr, 'staff')) {
            
            foreach ($jobResourceArr['staff'] as $sKey => $staff) {
                $jobResourceArr['staff'][$sKey]['totalTime'] = 0;
                
                // dd($jobResourceArr['staff']);
                
                if (array_get($staff, 'timesheet')) {
                    
                    foreach ($staff['timesheet'] as $sTKey => $timesheet) {
                        
                        $jobResourceArr['staff'][$sKey]['totalTime'] += (int) array_get($timesheet, 'duration');
                        
                        $jobTime['total'] += (int) array_get($timesheet, 'duration');
                        
                        if (array_get($timesheet, 'billable') == Config::get('constant.option.true'))
                            $jobTime['billable'] += (int) array_get($timesheet, 'duration');
                        else
                            $jobTime['nonbillable'] += (int) array_get($timesheet, 'duration');
                    }
                }
            }
            $jobResourceArr['time'] = $jobTime;
        }
    }
    /**
     *  Returns list of Jobs By ClientID
     *
     *  @param number $clientSiteId;
     *  @return App\Http\Models\Jobs | NULL
     */
    public function ajaxListByClientID(Request $request){
        if(! $request->has('client_id')){
            return \Response::json(array(
                'code' => trans('messages.portal.error.ajax.client_id_not_found.code'),
                'body' => trans('messages.portal.error.ajax.client_id_not_found.message')
            ), 412);
        }
        
        $clientObj = new Client();
        $clientObj = $clientObj->getByID($request->get('client_id'));
        
        if(! $clientObj instanceof Client){
            return \Response::json(array(
                'code' => trans('messages.portal.error.ajax.client_not_found.code'),
                'body' => trans('messages.portal.error.ajax.client_not_found.message')
            ), 404);
        }
        
        $clientJobObj = Job::getJobByClientId($clientObj->id);
        
        if( ! count($clientJobObj) ){
            return \Response::json(array(
                'code' => trans('messages.portal.error.ajax.no_client_site_found.code'),
                'body' => trans('messages.portal.error.ajax.no_client_site_found.message')
            ), 200);
        }
        
        return \Response::json(array(
            'code' => '200',
            'body' => $clientJobObj
        ), 200);
    }
}
