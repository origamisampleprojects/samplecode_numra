<?php
namespace App\Http\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use App\Http\Models\JobResource;
use App\Http\Models\JobNote;
use App\Http\Models\Document;

class Job extends Model
{
    use \DataMapper;

    protected $table = "job";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'workOrderNo',
        'purchaseOrderNo',
        'client_id',
        'client_site_id',
        'proposal_id',
        'title',
        'type',
        'description',
        'specialInstruction',
        'productType',
        'tankSize',
        'hoseLength',
        'openingSize',
        'drum',
        'shaftDepth',
        'personal',
        'personalEquip',
        'material',
        'equipment',
        'utilityCheck',
        'utilityNo',
        'permit',
        'notification',
        'inspection',
        'inspectionOn',
        'subcontractor',
        'jobMaterial',
        'followup_job_id',
        'scheduled_at',
        'status'
    ];

    /**
     * Belongs To Relation with Client
     */
    public function client()
    {
        return $this->belongsTo('App\Http\Models\Client', 'client_id', 'id');
    }

    /**
     * Belongs To Relation with Client Site
     */
    public function clientSite()
    {
        return $this->belongsTo('App\Http\Models\ClientSite', 'client_site_id', 'id');
    }

    /**
     * Belongs To Relation with Proposal
     */
    public function proposal()
    {
        return $this->belongsTo('App\Http\Models\Proposal', 'proposal_id', 'id');
    }

    /**
     * Has Many Relation with JobNotes
     */
    public function note()
    {
        return $this->hasMany('App\Http\Models\JobNote');
    }

    /**
     * One-to-Many Polymorphic Relation with Job Resources
     */
    public function resources()
    {
        // return $this->morphMany('App\Http\Models\JobResource', 'resource');
        return $this->leftJoin('job_resource', 'job_resource.job_id', '=', 'job.id')
            ->where('job_resource.job_id', '=', $this->id)
            ->select('job_resource.*')
            ->get();
    }

     /**
     * One-to-Many Polymorphic Relation with Document
     */
    public function document()
    {
        return $this->morphMany('App\Http\Models\Document', 'belongTo');
    }

    public function staff()
    {
        return $this->leftJoin('job_resource', 'job_resource.job_id', '=', 'job.id')
            ->where('job_resource.job_id', '=', $this->id)
            ->where('job_resource.resource_type', '=', Config::get('constant.resource.model.staff'))
            ->select('job_resource.*')
            ->get();
    }

    /**
     * List all Staff Members which haven't added their notes
     */
    public function StaffWithMissingNotes()
    {
        return $this->leftJoin('job_resource', 'job_resource.job_id', '=', 'job.id')
            ->leftJoin('job_note', function ($join) {
            $join->on('job_note.job_id', '=', 'job_resource.job_id');
            $join->on('job_note.staff_id', '=', 'job_resource.resource_id');
        })
            ->where('job_resource.job_id', '=', $this->id)
            ->where('job_resource.resource_type', '=', Config::get('constant.resource.model.staff'))
            ->groupBy('job_resource.resource_id')
            ->whereNull('job_note.staff_id')
            ->select([
            'job_resource.*'
        ])
            ->get();
    }

    public function getScheduleDateAttribute()
    {
        return $this->attributes['scheduled_at'] ? Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['scheduled_at'])->format('m/d/Y') : NULL;
    }

    //Functions to determine the status of current Job Object
    public function isPending()
    {
        return ($this->status == Config::Get('constant.status.pending') ? true : false);
    }

    public function isCancelled()
    {
        return ($this->status == Config::Get('constant.status.cancelled') ? true : false);
    }

    public function isInprogress()
    {
        return ($this->status == Config::Get('constant.status.inprogress') ? true : false);
    }

    public function isCompleted()
    {
        return ($this->status == Config::Get('constant.status.completed') ? true : false);
    }

    /**
     * Get Detail Info of all the allocated Resource for a Job
     */
    public function getResources()
    {
        $resourceArr = [];
        $resourceArr['staff'] = JobResource::where('job_id', '=', $this->id)->where('job_resource.resource_type', '=', Config::get('constant.resource.model.staff'))
            ->leftJoin('staff', 'job_resource.resource_id', '=', 'staff.id')
            ->select('staff.id', 'job_id', 'staff_id', 'ts_staff_id', 'staff.firstName', 'staff.lastName', 'staff.email', 'staff.jobTitle', 'staff.type', 'staff.profileImage')
            ->get()
            ->toArray();
        
        $resourceArr['vehicle'] = JobResource::where('job_id', '=', $this->id)->where('job_resource.resource_type', '=', Config::get('constant.resource.model.vehicle'))
            ->leftJoin('vehicle', 'job_resource.resource_id', '=', 'vehicle.id')
            ->get()
            ->toArray();
        
        $resourceArr['inventory'] = JobResource::where('job_id', '=', $this->id)->where('job_resource.resource_type', '=', Config::get('constant.resource.model.inventory'))
            ->leftJoin('inventory_item', 'job_resource.resource_id', '=', 'inventory_item.id')
            ->get()
            ->toArray();
        
        return $resourceArr;
    }

    /**
     * Get Job By ID
     *
     * @param number $jobId;            
     * @return App\Http\Models\Job | NULL
     */
    public function getByID($jobId)
    {
        return $this->where('id', '=', $jobId)->first();
    }
    
    /**
     * Get Job Detail By Job ID
     *
     * @param number $jobId;            
     * @return App\Http\Models\Job | NULL
     */
    public function getJobDetail($jobId)
    {
        return $this->where('job.id', '=', $jobId)
            ->leftJoin('client_site', function ($join) {
            $join->on('client_site.id', '=', 'job.client_site_id');
        })
            ->leftJoin('country', 'country.id', '=', 'client_site.country_id')
            ->leftJoin('state', 'state.id', '=', 'client_site.state_id')
            ->whereIn('job.status', [
            \Config::get('constant.status.inprogress'),
            \Config::get('constant.status.completed'),
            \Config::get('constant.status.pending')
        ])
            ->select([
            'job.id',
            'job.title',
            'job.type',
            'job.status',
            'job.scheduled_at',
            'job.description',
            'job.specialInstruction',
            'client_site.contactPerson',
            'client_site.contactNumber',
            'client_site.address',
            'country.name as country',
            'state.name as state',
            'state.code as stateCode',
            'client_site.zipcode',
            'client_site.city'
        ])
            ->first();
    }

    /**
    *   Set Value for Job Associated
    *
    *  @param number $clientId
    *  @param number $clientSiteId
    *  @param number $proposalId
    *  @param Date $scheduleDate
    *  @return this
    */
    public function setAssociationData($clientId, $clientSiteId, $proposalId, $scheduleDate)
    {
        $this->client_id = $clientId;
        $this->client_site_id = $clientSiteId;
        $this->proposal_id = $proposalId ?  : NULL;
        $this->scheduled_at = $scheduleDate;
        return $this;
    }

    /**
     * Allocate Resource to this particular job
     *
     * @param Array $jobResourcesData            
     */
    public function addNote($staffId, $action, $note)
    {
        return (new JobNote())->saveNote($this->id, $staffId, $action, $note);
    }

    /**
     * Allocate Staff Resource to this particular job
     *
     * @param number $staff_id
     *            Id of staff who is creating this screen
     */
    public function assignStaff($resourceId)
    {
        $jobResourceObj = (new JobResource())->getByJobResourceID($this->id, $resourceId, Config::get('constant.resource.model.staff'));
        
        if (! $jobResourceObj) {
            $jobResourceObj = (new JobResource())->assignResource($this->id, $resourceId, $resourceId, Config::get('constant.resource.model.staff'), 1);
        }
        return $jobResourceObj;
    }

    /**
     * Allocate Resource to this particular job
     *
     * @param Array $jobResourcesData
     * @param Bool $deleteNAddResources
     * @return App/Http/Models/Job Object          
     */
    public function allocatedResources($jobResourcesData, $deleteNAddResources = true)
    {
        return JobResource::saveAllocatedResources($this->id, $jobResourcesData, $deleteNAddResources);
    }

    /**
     * Get Job Count for a given Client
     *
     * @param number $clientId
     * @return Number          
     */
    public static function getJobCountByClientId($clientId)
    {
        return self::where('client_id', '=', $clientId)->count();
    }

    /**
     * Update status of Job
     *
     * @param string $status            
     * @return App/Http/Models/Job Object
     */
    public function updateStatus($status)
    {
        return $this->fillnSave([
            'status' => $status
        ]);
    }

    /**
     * Get Paginated List of Job
     *
     * @param string $search            
     * @param string $filter            
     * @param number $currentPage            
     * @param number $perPage            
     * @return Collection |NULL App/Http/Models/Job Object
     */
    public function getPaginatedList($search, $filter, $currentPage, $perPage)
    {
        return $this->with('client', 'clientSite')
            ->where(function ($query) use($search) {
            if ($search) {
                $query->orWhere('title', 'like', "%$search%");
                $query->orWhere('type', 'like', "%$search%");
            }
        })
            ->where(function ($query) use($filter) {
            if ($filter)
                $query->where('status', '=', $filter);
        })
            ->paginate($perPage, array(
            '*'
        ), 'page', $currentPage);
    }

    /**
     * Save Job Associated Document
     *
     * @param Array $data            
     * @return Collection |NULL App/Http/Models/Document Object
     */
    public function addAssociatedFiles($data)
    {
        $data['client_id'] = $this->client_id;
        $data['belongTo_id'] = $this->id;
        $data['belongTo_type'] = get_class($this);
        return (new \App\Http\Models\Document())->saveFiles($data);
    }

    /**
     * Get Staff's Status for given Job
     *
     * @param number $jobId          
     * @param number $staffId           
     * @param Bool $status            
     * @return Bool
     */
    public static function getStaffJobCurrentStatus($jobId, $staffId, $status)
    {
        return (JobNote::isJobCompletionNoteAdded($jobId, $staffId)) ? Config::get('constant.status.completed') : $status;
    }

    /**
     * Get Job list for a given Client
     *
     * @param number $clientId            
     */
    public static function getJobByClientId($clientId)
    {
        return self::where('client_id', '=', $clientId)
            ->where('status', '=', \Config::get('constant.status.completed'))
            ->select('title', 'id')
            ->get();
    }
}
